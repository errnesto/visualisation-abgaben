var gulp = require('gulp')
var gutil = require('gulp-util')
var source = require('vinyl-source-stream')
var babelify = require('babelify')
var watchify = require('watchify')
var exorcist = require('exorcist')
var browserify = require('browserify')
var browserSync = require('browser-sync').create()

// Input file.
watchify.args.debug = true
var bundler = (process.argv[3] && process.argv[3] === '--deploy')
  ? browserify('./public/js/app.js', watchify.args)
  : watchify(browserify('./public/js/app.js', watchify.args))

// Babel transform
bundler.transform(babelify.configure({
  sourceMapRelative: 'public/js',
  presets: ['es2015']
}))

// On updates recompile
bundler.on('update', bundle)

function bundle () {
  gutil.log('Compiling JS...')

  return bundler.bundle()
    .on('error', function (err) {
      gutil.log(err.message)
      browserSync.notify('Browserify Error!')
      this.emit('end')
    })
    .pipe(exorcist('./public/js/dist/bundle.js.map'))
    .pipe(source('bundle.js'))
    .pipe(gulp.dest('./public/js/dist'))
    .pipe(browserSync.stream({once: true}))
}

/**
 * Gulp task alias
 */
gulp.task('bundle', function () {
  return bundle()
})

/**
 * First bundle, then serve from the ./public directory
 */
gulp.task('default', ['bundle'], function () {
  browserSync.init({
    server: './public'
  })

  gulp.watch('./public/{index.html,css/*}', browserSync.reload)
})
