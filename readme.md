# Visualisation abgaben

# install
you need to have [npm](https://www.npmjs.com/) and [gulp](http://gulpjs.com/) installed then just run `npm install` to install all other dependencies.

# run
run `gulp` to bundle and open the browser or `gulp bundle --deploy` to just bundle the .js files
