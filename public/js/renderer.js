'use strict'
import d3 from 'd3'

class Renderer {

  constructor ({ svgContainer, settings = {} }) {
    this.svg = d3.select(svgContainer)
    this.width = svgContainer.getBoundingClientRect().width
    this.height = svgContainer.getBoundingClientRect().height

    this.setSettings(settings)

    this.draw()
  }

  _addDistractorsTo (glyphs) {
    let numberOfDistractors = this.settings.numberOfDistractors
    let distractorsProperties = this.settings.distractorsProperties
    let distractorIndex = 0
    let offset = 0

    for (let i = 0; i < numberOfDistractors; i++) {
      let share = offset + distractorsProperties[distractorIndex].share
      if (i >= share * numberOfDistractors) {
        offset = share
        distractorIndex++
      }

      if (distractorIndex >= distractorsProperties.length) throw new Error('invald distractor shares')

      let distractor = distractorsProperties[distractorIndex].props
      glyphs.push(Object.create(distractor))
    }

    return glyphs
  }

  _setAtribuesFor ({ svgElements, form }) {
    switch (form) {
      case 'circle':
        svgElements
          .attr('class', glyph => glyph.class)
          .attr('r', glyph => glyph.radius)
          .attr('cx', glyph => glyph.x)
          .attr('cy', glyph => glyph.y)
          .attr('fill', glyph => glyph.color)
        break
      case 'rect':
        svgElements
          .attr('class', glyph => glyph.class)
          .attr('width', glyph => glyph.width)
          .attr('height', glyph => glyph.height)
          .attr('fill', glyph => glyph.color)
          .attr('x', glyph => glyph.x)
          .attr('y', glyph => glyph.y)
        break
      case 'line':
        svgElements
          .attr('class', glyph => glyph.class)
          .attr('stroke', glyph => glyph.color)
          .attr('x1', glyph => glyph.x - 5)
          .attr('y1', glyph => glyph.y - 5)
          .attr('x2', glyph => glyph.x + 5)
          .attr('y2', glyph => glyph.y + 5)
          .attr('transform', glyph => `rotate(${ glyph.rotate }, ${ glyph.x }, ${ glyph.y })`)
        break
    }
  }

  draw () {
    this.show(false)
    this.svg.selectAll('*').remove()

    let elementForms = ['circle', 'rect', 'line']
    let glyphs = []

    this._addDistractorsTo(glyphs)
    // add main object 50% of the time
    if (Math.random() < 0.5) {
      glyphs.push(this.settings.mainObjectProperties)
    }

    // add svg elements
    elementForms.forEach(elementForm => {
      this.svg.selectAll(elementForm)
        .data(glyphs.filter(glyph => glyph.form === elementForm))
        .enter()
        .append(elementForm)
    })

    let force = d3.layout.force()
      .nodes(glyphs)
      .charge(this.settings.charge)
      .size([this.width, this.height])
      .on('tick', e => {
        elementForms.forEach(elementForm => {
          let elements = this.svg.selectAll(elementForm)
          this._setAtribuesFor({ svgElements: elements, form: elementForm })
        })
      })

    force.start()
    for (let i = 0; i < 50; ++i) force.tick()
    force.stop()
  }

  setSettings (settings) {
    // merge defaultSettings with settings parameter
    this.settings = {}
    for (let attrname in Renderer.defaultSettings) { this.settings[attrname] = Renderer.defaultSettings[attrname] }
    for (let attrname in settings) { this.settings[attrname] = settings[attrname] }
  }

  blink () {
    let start = null
    let blink = (timestamp) => {
      if (!start) start = timestamp
      let progress = timestamp - start
      this.show(true)
      if (progress < this.settings.blinkMilliseconds) {
        window.requestAnimationFrame(blink)
      } else {
        this.show(false)
      }
    }
    window.requestAnimationFrame(blink)
  }

  show (show) {
    this.svg.style('opacity', show ? 1 : 0)
  }
}

Renderer.defaultSettings = {
  charge: -30,
  blinkMilliseconds: 250,
  numberOfDistractors: 100,
  distractorsProperties: [{ share: 1, props: { form: 'circle', color: 'blue', radius: '10px' } }],
  mainObjectProperties: { form: 'circle', color: 'red', radius: '10px' }
}

export default Renderer
