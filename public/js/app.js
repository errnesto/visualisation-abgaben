'use strict'

import Renderer from './renderer.js'

var elementwithId = document.getElementById.bind(document)

let svg = elementwithId('svg')
let showSelect = elementwithId('show-select')
let blinkButton = elementwithId('blink-button')
let nextButton = elementwithId('next-button')

let tests = [
  // color
  {
    numberOfDistractors: 5,
    distractorsProperties: [
      { share: 1, props: { form: 'circle', color: 'blue', radius: '10px' } }
    ]
  },
  {
    numberOfDistractors: 100,
    distractorsProperties: [
      { share: 0.5, props: { form: 'circle', color: 'blue', radius: '10px' } },
      { share: 0.5, props: { form: 'line', color: 'blue', rotate: '45' } }
    ]
  },
  // size
  {
    numberOfDistractors: 200,
    distractorsProperties: [
      { share: 1, props: { form: 'rect', color: 'blue', width: '10px', height: '10px' } }
    ],
    mainObjectProperties: { form: 'rect', color: 'blue', width: '20px', height: '20px' }
  },
  // form
  {
    numberOfDistractors: 50,
    distractorsProperties: [
      { share: 0.5, props: { form: 'rect', color: 'blue', width: '10px', height: '10px' } },
      { share: 0.5, props: { form: 'line', color: 'blue', rotate: '45' } }
    ],
    mainObjectProperties: { form: 'circle', color: 'blue', radius: '10px' }
  },
  {
    numberOfDistractors: 50,
    distractorsProperties: [
      { share: 0.25, props: { form: 'rect', color: 'blue', width: '10px', height: '10px' } },
      { share: 0.25, props: { form: 'line', color: 'blue', rotate: '45' } },
      { share: 0.25, props: { form: 'rect', color: 'blue', width: '20px', height: '20px' } },
      { share: 0.25, props: { form: 'line', color: 'blue', rotate: '0' } }
    ],
    mainObjectProperties: { form: 'circle', color: 'blue', radius: '10px' }
  },
  // combined
  {
    numberOfDistractors: 40,
    distractorsProperties: [
      { share: 0.5, props: { form: 'circle', color: 'blue', radius: '10px' } },
      { share: 0.5, props: { form: 'circle', color: 'red', radius: '20px' } }
    ],
    mainObjectProperties: { form: 'circle', color: 'blue', radius: '20px' },
    charge: -90
  },
  {
    numberOfDistractors: 100,
    distractorsProperties: [
      { share: 0.5, props: { form: 'circle', color: 'blue', radius: '10px' } },
      { share: 0.5, props: { form: 'circle', color: 'red', radius: '15px' } }
    ],
    mainObjectProperties: { form: 'circle', color: 'blue', radius: '15px' },
    charge: -90
  }
]

let renderer = new Renderer({ svgContainer: svg })

showSelect.addEventListener('click', (e) => {
  renderer.show(e.target.checked)
})

blinkButton.addEventListener('click', () => {
  renderer.blink()
})

let testIndex = 0
nextButton.addEventListener('click', () => {
  showSelect.checked = false
  renderer.setSettings(tests[testIndex])
  renderer.draw()
  testIndex += 1
  if (testIndex >= tests.length) nextButton.disabled = true
})
